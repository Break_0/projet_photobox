/**
* Launois / Hodot
* Photoloader
*/

'use strict'

import photoloader from "./photoloader.js" ;
import lightbox from "./lightbox.js" ;

let first ;
let last ;
let next ;
let previous ;
let nbrImageGallery ;

/**
* Fonciton qui initialise la Gallery et le photoloader qu'elle utilisera.
*/
function initialisation(url_serveur, uri_base){
  photoloader.initialisation(url_serveur);
  first = uri_base;
}

/**
* Fonction qui change les uri des autres gallery en fonction du JSON passé en parametres
*/
function configuration(reponse) {
    next = reponse.data.links.next.href;
    previous = reponse.data.links.prev.href;
    first = reponse.data.links.first.href;
    last = reponse.data.links.last.href;
    return reponse;
}

/*
* Fonction qui charge une gallery de base avec photoloader
* puis l'affiche dans le DOM
* parametres : > pour next
*              < pour previous
*              l pour load
*/
function loadGallery(direction){
  let gallery;
  switch(direction){
    case ">" : //next
      gallery = photoloader.listeObjPromesse(next);
      console.log(next);
      break;
    case "<" : //previous
      gallery = photoloader.listeObjPromesse(previous);
      break;
    case "l" : //load
      gallery = photoloader.listeObjPromesse(first);
      break;
  }
  return gallery.then(configuration).then(traitementPhotos);
}



/*
 Fonction qui pour chaque photo demande un affichage
         a.addEventListener('click',function (){
            lightbox.creer(a.firstElementChild,a.lastElementChild);
            lightbox.modal();
        });
*/
function traitementPhotos(jsonGallery){
  nbrImageGallery = 0 ;
  let gallery = jsonGallery.data.photos.reduce( (g, p) =>
    g.append(`<div class='vignette' id="vignette${++nbrImageGallery}">
                  <img data-img='${photoloader.getUrlServ()+p.photo.original.href}'
                        data-uri='${p.links.self.href}'
                        src= '${photoloader.getUrlServ()+p.photo.thumbnail.href }'/>
                       <div>${p.photo.titre}</div>
                    </div>`
                ),
      $('<div class="gallery-container" id="gallery-container"></div>')
  );

  let gallery_container=$('#gallery');
  gallery_container.find('.gallery-container').remove();
  gallery_container.prepend(gallery);
  // Pour faire passer le nbr d'image dans la gallery
    $(".vignette").click( function (e)  {
        lightbox.creer( $(e.currentTarget) )
    });

}

function getNbrImageGallery(){
  return nbrImageGallery;
}

export default {
  initialisation : initialisation,
  loadGallery : loadGallery,
  getNbrImageGallery : getNbrImageGallery
}
