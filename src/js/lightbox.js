'use strict';

import gallery from './gallery.js';
import photoloader from './photoloader.js';

let img;
let titre;
/**
* Fonction qui prend un $() en parametres
*/
function creer(e) {
    let img = e.children("img").first();
    let src = img.data("img");
    let titre = e.children("div").first().text();
    //console.log(src)
    // On vide la lightbox
    $("#lightbox_container").empty();
    //On crée un nouveau contenu
    let lightbox = `<div id="lightbox">
        <div id="lightbox-head">
            <p id="lightbox_close">   -X-  </p>
            <h1 id="lightbox_title">${titre}</h1>
        </div>
        <h1 id="lightbox_prev"> prev photo </h1>
        <h1 id="lightbox_next"> next photo </h1>
        <div id="lightbox-img">
            <img id="lightbox_full_img" src="${src}">
        </div>
        <div id="lightbox-description">
                
            </div>
    </div>`;
    //On remplit la lightbox
    $("#lightbox_container").append(lightbox);
    $("#lightbox_close").click(fermerLightbox); // fermer la fenetre
    $("#lightbox_next").click( () => next(e) ); // prochaine lightbox
    $("#lightbox_prev").click( () =>prev(e) ); // precedente lightbox
    console.log(e);
    descr(img.data("uri"));
    //on l'affiche
    ouvrirLightBox();
}

function next(e){
  if( e.attr("id").endsWith(""+gallery.getNbrImageGallery()) ){ //C'est la derniere image de la gallery
    gallery.loadGallery(">").then( () =>{
    $("#vignette1").click();
  });
  }
  else{ // il y a encore des images apres
    creer( e.next() );
  }
}

function prev(e){
  if(e.attr("id") == "vignette1"){
    gallery.loadGallery("<").then( () =>{
      let nbr = gallery.getNbrImageGallery();
      $("#vignette"+nbr).click();
    });
  }
  else{
    creer ( e.prev() );
  }
}

function ouvrirLightBox(){
    $("#lightbox_container").css("display", "block");
}

function fermerLightbox() {
    $("#lightbox_container").css("display", "none");
}

function descr(urlPhoto){
    let info=photoloader.listeObjPromesse(urlPhoto);

    info.then(function (rep) {

        let values = rep.data.photo;
        $("#lightbox-description").append($('<div id="description"><h1>Titre :'+values.titre+'</h1> ' +
            '<h1>Fichier : '+values.file+'</h1> ' +
            '<h1>Description : '+values.descr+'</h1> ' +
            '<h1>Format : '+values.format+'</h1></div>'))
    })

};

export default {
    ouvrir : ouvrirLightBox,
    fermer : fermerLightbox,
    creer : creer
}


