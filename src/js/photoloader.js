/**
* Launois / Hodot
* Photoloader
*/

'use strict'

var url_serveur;

/**
* Fonction qui initialise le nom du serveur
* modifie url_serveur
*/
function initialisation(addServeur){
    url_serveur=addServeur;
}

/**
* Fonction qui renvoit un JSON de la liste de photo demandée
 * @param url
 * @returns {Promise<T | never>}
 */
function listeObjPromesse(uri) {
    return axios.get(url_serveur+uri,{
        responseType: 'json',
            withCredentials : true
    }).catch(catch_error)
}

function catch_error(e) {
    console.log(e);
}

function getUrlServ(){
    return url_serveur;
}

//Liste des exports
export default {
    initialisation : initialisation,
    listeObjPromesse : listeObjPromesse,
    getUrlServ : getUrlServ
}
